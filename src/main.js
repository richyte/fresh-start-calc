// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import Toasted from 'vue-toasted'
import VTooltip from 'v-tooltip'

Vue.use(VTooltip)
Vue.use(Toasted, {
    iconPack : 'material'
})
Vue.config.productionTip = false

Vue.toasted.register('too_many_trees', 'Can\'t choose more than 3 skill trees', {
    type : 'error',
    icon : 'clear',
	duration : 3000
})
Vue.toasted.register('not_enough_points', 'You don\'t have enough skill points', {
    type : 'error',
    icon : 'clear',
	duration : 3000
})
Vue.toasted.register('share_link_copied', 'Share link copied', {
    type : 'success',
    icon : 'done',
	duration : 3000
})
Vue.toasted.register('saved', 'Saved your build', {
    type : 'success',
    icon : 'done',
	duration : 3000
})
Vue.toasted.register('save_loaded', 'Loaded your save', {
    type : 'success',
    icon : 'done',
	duration : 3000
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
